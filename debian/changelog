simple-obfs (0.0.5-7) UNRELEASED; urgency=low

  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.5.0, no changes needed.

 -- Debian Janitor <janitor@jelmer.uk>  Sun, 26 Jul 2020 12:03:42 -0000

simple-obfs (0.0.5-6) unstable; urgency=medium

  * debian/patches:
    - 0005: Cherry-pick from upstream to fix FTBFS under GCC-8
    - 0006: fix FTBFS under GCC-9 (Closes: #925829).

 -- Roger Shimizu <rogershimizu@gmail.com>  Sat, 17 Aug 2019 14:49:44 +0900

simple-obfs (0.0.5-5) unstable; urgency=medium

  * Team upload.
  * debian/control: Bump Standards-Version to 4.3.0.
  * debian/rules: Use "dh_missing --fail-missing".
  * debian/postrm: Make the scripts more robust to fix postrm
    error on purge.

 -- Boyuan Yang <byang@debian.org>  Fri, 01 Mar 2019 11:27:48 -0500

simple-obfs (0.0.5-4) unstable; urgency=medium

  * debian/patches: Backport patches from upstream.
    - [f4e03d9c] failover need set port
    - [40f56bea] Update obfs-server.asciidoc
  * debian/control:
    - Use tracker mail as maintainer address.

 -- Roger Shimizu <rogershimizu@gmail.com>  Sat, 16 Jun 2018 17:57:44 +0900

simple-obfs (0.0.5-3) unstable; urgency=medium

  * debian/control:
    - Migrate Vcs-* from alioth to salsa.

 -- Roger Shimizu <rogershimizu@gmail.com>  Sun, 10 Jun 2018 21:14:59 +0900

simple-obfs (0.0.5-2) unstable; urgency=medium

  * debian/patches:
    - Remove the temp fix to set TZ=UTC prefix for asciidoc command
      for reproducible build.
    - Refresh patches.

 -- Roger Shimizu <rogershimizu@gmail.com>  Mon, 04 Dec 2017 21:38:36 +0900

simple-obfs (0.0.5-1~bpo8+1) jessie-backports-sloppy; urgency=medium

  * Rebuild for jessie-backports-sloppy.

 -- Roger Shimizu <rogershimizu@gmail.com>  Sat, 02 Dec 2017 13:21:02 +0900

simple-obfs (0.0.5-1~bpo9+1) stretch-backports; urgency=medium

  * Rebuild for stretch-backports.

 -- Roger Shimizu <rogershimizu@gmail.com>  Sat, 02 Dec 2017 13:17:45 +0900

simple-obfs (0.0.5-1) unstable; urgency=medium

  * New upstream release 0.0.5
  * debian/watch:
    - Change filenamemangle to match with the filename in archive.
  * debian/patches:
    - Refresh patches.
  * debian/control:
    - Remove Build-Depends: libudns-dev.
    - Bump up Standards-Version to 4.1.1 (no change required).
  * debian/patches:
    - Backport bits from upstream:
      + [c7ae0e6] Remove c-ares dependency in README.
  * debian/copyright:
    - Add new file entry: m4/inet_ntop.m4.
    - Remove entry: src/resolv.[ch], and m4/ax_pthread.m4.

 -- Roger Shimizu <rogershimizu@gmail.com>  Thu, 23 Nov 2017 21:22:09 +0900

simple-obfs (0.0.3-5~bpo8+1) jessie-backports-sloppy; urgency=medium

  * Rebuild for jessie-backports-sloppy.

 -- Roger Shimizu <rogershimizu@gmail.com>  Wed, 04 Oct 2017 23:00:11 +0900

simple-obfs (0.0.3-5~bpo9+1) stretch-backports; urgency=medium

  * Rebuild for stretch-backports.

 -- Roger Shimizu <rogershimizu@gmail.com>  Wed, 04 Oct 2017 22:54:41 +0900

simple-obfs (0.0.3-5) unstable; urgency=medium

  * debian/patches:
    - Backport patch to update json library.

 -- Roger Shimizu <rogershimizu@gmail.com>  Sat, 23 Sep 2017 01:03:15 +0900

simple-obfs (0.0.3-4~bpo8+1) jessie-backports-sloppy; urgency=medium

  * Rebuild for jessie-backports-sloppy.

 -- Roger Shimizu <rogershimizu@gmail.com>  Sat, 12 Aug 2017 00:46:27 -0400

simple-obfs (0.0.3-4~bpo9+1) stretch-backports; urgency=medium

  * Rebuild for stretch-backports.

 -- Roger Shimizu <rogershimizu@gmail.com>  Sat, 12 Aug 2017 00:43:27 -0400

simple-obfs (0.0.3-4) unstable; urgency=medium

  * Add config.json support.
  * debian/patches:
    - Backport a few patches from upstream.
  * debian/control:
    - Remove libsodium-dev dependency.

 -- Roger Shimizu <rogershimizu@gmail.com>  Sun, 06 Aug 2017 16:50:12 -0400

simple-obfs (0.0.3-3~bpo8+1) jessie-backports-sloppy; urgency=medium

  * Rebuild for jessie-backports-sloppy.

 -- Roger Shimizu <rogershimizu@gmail.com>  Sun, 16 Jul 2017 02:46:40 +0900

simple-obfs (0.0.3-3~bpo9+1) stretch-backports; urgency=medium

  * Rebuild for stretch-backports.

 -- Roger Shimizu <rogershimizu@gmail.com>  Sun, 16 Jul 2017 02:41:11 +0900

simple-obfs (0.0.3-3) unstable; urgency=medium

  * debian/patches:
    - Refresh patch.
    - Add a patch to fix reproducible build.
    - Cherry-pick two upstream patches to better work with reverse
      proxy.

 -- Roger Shimizu <rogershimizu@gmail.com>  Sun, 09 Jul 2017 23:30:04 +0900

simple-obfs (0.0.3-2) unstable; urgency=medium

  * debian/rules:
    - Add "--disable-ssp" option when dh_auto_configure, to fix FTBFS
      for alpha/hppa arch. Whether to have stack-protector is decided
      by dpkg-buildflags. So it should be safe here.
      See Bug in parent project: #829498.

 -- Roger Shimizu <rogershimizu@gmail.com>  Fri, 12 May 2017 19:37:41 +0900

simple-obfs (0.0.3-1) unstable; urgency=medium

  * Initial release. (Closes: #858370)

 -- Roger Shimizu <rogershimizu@gmail.com>  Wed, 19 Apr 2017 21:38:03 +0900
